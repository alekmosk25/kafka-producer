# Kafka Producer

Apache Kafka, a distributed streaming platform, is a valuable tool for handling data streams. here you can find an
implemented a Kafka producer in C++. This will enable you to efficiently send data to a Kafka topics.

## Getting Started

To get started with the Kafka producer implementation, follow these steps:

### Prerequisites

Before you begin, make sure you have the following prerequisites installed on your system:

- C++ compiler
- librdkafka C/C++ library
- CMake
- GTest and GMock (for testing)

You can install most of these dependencies on an Ubuntu 22.04 LTS system using the provided script in the blog post.

### Installation

- Clone this repository to your local machine:
    ```shell
    git clone https://gitlab.com/advt3/kafka-producer.git
    cd kafka-producer
    ```
- Build your project
    ```shell
    cmake -S . -B build
    cmake --build build
    ```

#### Testing

- Start a local Kafka broker with the docker-compose.yml file
    ```bash
    docker-compose up
    ```
- Run the tests
    ```bash
    ./build/tests/kafka_producer_test
    ```

### Docker Build

- Build and run the container

```bash
docker build -t build -f build.dockerfile .
docker run -it --rm -v $PWD:/src build 
```

- Build the code using the /opt folder to not save the binaries

```bash
cmake -S /src -B build
cmake --build build
```

To run the test you would need to setup the network of the containers.

## License

This project is licensed under the [MIT License](https://opensource.org/licenses/MIT)