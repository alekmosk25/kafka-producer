//
// Created by Santiago Hurtado on 20.09.23.
//
#include "IProducer.h"
#include <librdkafka/rdkafkacpp.h>
#include <memory>
#include <iostream>
#include <stdexcept>

class KafkaProducer : public IProducer {
private:
    std::unique_ptr<RdKafka::Producer> kafkaProducer;

public:

    explicit KafkaProducer(const std::string &brokers) {
        try {
            std::unique_ptr<RdKafka::Conf> conf(RdKafka::Conf::create(RdKafka::Conf::CONF_GLOBAL));
            std::string error_str;
            conf->set("bootstrap.servers", brokers, error_str);

            // Create Kafka producer
            std::unique_ptr<RdKafka::Producer> producer(RdKafka::Producer::create(conf.get(), error_str));
            if (!producer) {
                throw std::runtime_error("Failed to create producer: " + error_str);
            }

            // Check connection to Kafka broker
            RdKafka::Metadata *metadata = nullptr;
            RdKafka::ErrorCode metadataError = producer->metadata(true, nullptr, &metadata, 5000);
            delete metadata;
            if (metadataError != RdKafka::ERR_NO_ERROR) {
                throw std::runtime_error("Failed to create producer: " + error_str);
            }
        }
        catch (const std::exception &ex) {
            std::cerr << "Exception during Kafka producer initialization: " << ex.what() << std::endl;
            throw; // Rethrow the exception to indicate a failure in the constructor
        }
    }

    void sendMessage(const std::string &topic, const std::string &message) override {
        try {
            RdKafka::ErrorCode producerError = kafkaProducer->produce(
                    topic,
                    RdKafka::Topic::PARTITION_UA,
                    RdKafka::Producer::RK_MSG_COPY,
                    const_cast<char *>(message.c_str()), message.size(),
                    /* Key */
                    NULL, 0,
                    /* Timestamp (defaults to current time) */
                    0,
                    /* Message headers, if any */
                    NULL,
                    /* Per-message opaque value passed to
                     * delivery report */
                    NULL
            );

            if (producerError != RdKafka::ERR_NO_ERROR) {
                throw std::runtime_error("Failed to produce message: " + RdKafka::err2str(producerError));
            }

            kafkaProducer->flush(10 * 1000);

            if (kafkaProducer->outq_len() > 0) {
                std::cerr << "% " << kafkaProducer->outq_len()
                          << " message(s) were not delivered" << std::endl;
            }
        } catch (const std::exception &ex) {
            std::cerr << "Exception during message sending: " << ex.what() << std::endl;
            // You can choose to log or handle this exception as needed
        }
    }

};