#include <iostream>
#include <memory>
#include "KafkaProducer.h"

int main() {
    try {
        std::string brokers = "thinkstation:9094";
        std::unique_ptr<IProducer> producer = std::make_unique<KafkaProducer>(brokers);
        producer->sendMessage("my-topic", "Hello, Kafka!");
    }
    catch (const std::exception &ex) {
        std::cerr << "Exception in main: " << ex.what() << std::endl;
        return 1;
    }
    return 0;
}
