cmake_minimum_required(VERSION 3.22)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

include(FetchContent)

FetchContent_Declare(
        googletest
        URL https://github.com/google/googletest/archive/03597a01ee50ed33e9dfd640b249b4be3799d395.zip
)
FetchContent_MakeAvailable(googletest)

project(test_project)
include_directories(../)
enable_testing()

include(GoogleTest)

add_executable(kafka_producer_test
        KafkaProducerTest.cpp
        ProducerTest.cpp)

target_link_libraries(kafka_producer_test
        gtest_main
        gmock_main
        RdKafka::rdkafka++)  # Link RdKafka for testing

target_include_directories(kafka_producer_test PRIVATE ${CMAKE_SOURCE_DIR}/.. ${GTEST_INCLUDE_DIRS})

gtest_discover_tests(kafka_producer_test)
