#include "gtest/gtest.h"
#include "KafkaProducer.h"

class KafkaProducerTest : public ::testing::Test {
protected:
    void SetUp() override {
        producer = std::make_unique<KafkaProducer>("localhost:9094");
    }

    void TearDown() override {
        producer.reset();
    }

    std::unique_ptr<KafkaProducer> producer;
};

TEST_F(KafkaProducerTest, SendMessageValid) {
    ASSERT_NO_THROW(producer->sendMessage("test-topic", "Hello, Kafka!"));
}

TEST_F(KafkaProducerTest, SendMessageInvalidTopic) {
    ASSERT_THROW(producer->sendMessage("", "Invalid Topic"), std::runtime_error);
}