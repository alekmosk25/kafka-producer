//
// Created by Santiago Hurtado on 21.09.23.
//
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "IProducer.h"

class MockProducer : public IProducer {
public:
    MOCK_METHOD(void, sendMessage, (const std::string& topic, const std::string& message), (override));
};

class ProducerTest : public ::testing::Test {
protected:
    void SetUp() override {
        producer = std::make_unique<MockProducer>();
    }

    void TearDown() override {
        producer.reset();
    }

    std::unique_ptr<MockProducer> producer;
};

TEST_F(ProducerTest, SendMessageValid) {
    EXPECT_CALL(*producer, sendMessage("test-topic", "Hello, Kafka!"))
            .Times(1);
    producer->sendMessage("test-topic", "Hello, Kafka!");
}

TEST_F(ProducerTest, SendMessageInvalidTopic) {
    EXPECT_CALL(*producer, sendMessage("", "Invalid Topic"))
            .Times(1);
    producer->sendMessage("", "Invalid Topic");
}

