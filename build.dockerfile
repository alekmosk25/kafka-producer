FROM ubuntu:22.04

ENV LIBRDKAFKA_VERSION="v2.2.0"

ENV LD_LIBRARY_PATH=/usr/local/lib/:$LD_LIBRARY_PATH

RUN apt-get update && apt-get install -y \
    git \
    build-essential \
    cmake \
    clang \
    libcurl4-openssl-dev \
    liblz4-dev \
    zlib1g-dev \
    libssl-dev \
    libsasl2-dev

WORKDIR /build

RUN git clone https://github.com/confluentinc/librdkafka.git librdkafka \
    && cd librdkafka && cmake -H. -B_cmake_build && cmake --build _cmake_build --target install

WORKDIR /opt

CMD ["/bin/bash"]
