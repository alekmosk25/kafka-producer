//
// Created by Santiago Hurtado on 18.09.23.
//

#ifndef KAFKA_PRODUCER_IPRODUCER_H
#define KAFKA_PRODUCER_IPRODUCER_H
#include <string>

class IProducer {
public:
    virtual void sendMessage(const std::string& topic, const std::string& message) = 0;
    virtual ~IProducer() = default;
};
#endif //KAFKA_PRODUCER_IPRODUCER_H
